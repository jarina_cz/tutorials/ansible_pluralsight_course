# ansible_pluralsight_course
https://app.pluralsight.com/library/courses/ansible-windows-getting-started

## LAB Environment

Hypervisor: Hyper-V

Firewall: pfsense (access to net, no access to PC, limited access from net)</br>
DC: dc.jarvis.com - Windows Server 2012 R2 (DNS/DHCP/AD)</br>
ansible control: master.jarvis.com - Ubuntu 18.04</br>
webserver: server2.jarvis.com - Windows Server 2016</br>
webserver: server3.jarvis.com - Windows Server 2016</br>
dbserver: server1.jarvis.com - Windows Server 2012 R2</br>

## Configure WinRM on Windows:

### Using GPO from AD
https://www.infrasightlabs.com/how-to-enable-winrm-on-windows-servers-clients

### Ansible way
https://docs.ansible.com/ansible/2.5/user_guide/windows_setup.html#winrm-setup

## Some links

### MODULES
- [List of Windows specific Modules](https://docs.ansible.com/ansible/2.5/modules/list_of_windows_modules.html)

## Ansible Tower (AWX)

### AWX
- Is an opensourced upstream version of Ansible Tower
- Quite easy to install

https://computingforgeeks.com/install-ansible-awx-on-centos-7-fedora-with-nginx-reverse-proxy-letsencrypt/

### Tower
- Is a paid product (Free up to 10 servers)
- Also quite easy installation

https://docs.ansible.com/ansible-tower/latest/html/quickinstall/index.html